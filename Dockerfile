FROM alpine:3.9

ARG VERSION=RELEASE_0_9_12_1

RUN apk add libpng-dev \
    zlib-dev \
    jpeg-dev \
    tiff-dev \
    boost-dev \
    cmake \
    gcc \
    g++ \
    libxrender-dev \
    fontconfig-dev \
    libpthread-stubs \
    libx11-dev \
    mesa-dev \
    build-base \
    qt-dev \
    icu-dev \
    git

#RUN git clone --branch ${VERSION} https://github.com/4lex4/scantailor-advanced.git /src/scantailor
RUN git clone --branch ${VERSION} https://github.com/scantailor/scantailor.git /src/scantailor
RUN cd /src/scantailor && mkdir build && cd build && cmake -G "Unix Makefiles" --build .. && make -j 6 && make install

FROM alpine:3.9
RUN apk add libpng \
    zlib \
    jpeg \
    tiff \
    boost \
    libxrender \
    fontconfig \
    libpthread-stubs \
    libx11 \
    mesa \
    qt \
    qt-x11 \
    icu

ARG UID=1000
ARG GID=1000
RUN addgroup -g ${GID} -S scantailor && \
    adduser -u ${UID} -S scantailor -G scantailor
RUN mkdir /incoming && mkdir /output && chown scantailor.scantailor /output
COPY --from=0 /usr/local/bin/scantailor-cli /usr/local/bin/
COPY --from=0 /usr/local/share/scantailor /usr/local/share/
USER scantailor
ENTRYPOINT ["/usr/local/bin/scantailor-cli" ,"/incoming", "/output"]
